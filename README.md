# Average and median annual household income, by income source, for households receiving income from this source, year ended June 2017

## Currently does

* Mean All House Cost
* Median All House Cost

## Currently does not
* Mean City House Cost
* Median City House Cost

* Mean Year House Cost
* Median Year House Cost

* Mean City and year House Cost
* Median City and year House Cost


# Result (23/03/2018)

## Summary:

Overall:
Mean All House Cost: $33,775.0
Median All House Cost: $17,241.0

## By Country:

Auckland region
Mean Cost: $38,776.5
Median Cost: $8,118.0

Canterbury region
Mean Cost: $32,826.1
Median Cost: $7,329.0

Rest of North Island
Mean Cost: $29,256.1
Median Cost: $7,800.0

Rest of South Island
Mean Cost: $30,332.4
Median Cost: $6,575.0

Total New Zealand
Mean Cost: $33,737.0
Median Cost: $7,424.0

Wellington region
Mean Cost: $37,722.0
Median Cost: $6,400.0

## By Year:

2007
Mean Cost: $27,302.2
Median Cost: $5,709.0

2008
Mean Cost: $30,241.6
Median Cost: $5,960.0

2009
Mean Cost: $31,267.3
Median Cost: $6,575.0

2010
Mean Cost: $31,062.0
Median Cost: $6,398.0

2011
Mean Cost: $32,448.0
Median Cost: $7,149.0

2012
Mean Cost: $32,245.4
Median Cost: $5,720.0

2013
Mean Cost: $34,400.7
Median Cost: $6,245.0

2014
Mean Cost: $36,459.7
Median Cost: $8,743.0

2015
Mean Cost: $37,570.3
Median Cost: $7,329.0

2016
Mean Cost: $38,322.6
Median Cost: $8,743.0

2017
Mean Cost: $40,205.7
Median Cost: $8,459.0

## By Type:

Self-employment
Mean Cost: $33,542.1
Median Cost: $0.0

Investments
Mean Cost: $7,281.1
Median Cost: $94.0

Private superannuation
Mean Cost: $11,576.8
Median Cost: $0.0

Other government benefits
Mean Cost: $7,607.3
Median Cost: $0.0

Wages and salaries
Mean Cost: $66,275.9
Median Cost: $55,035.0

New Zealand Superannuation and war pensions
Mean Cost: $13,823.6
Median Cost: $0.0

Other regular sources
Mean Cost: $8,365.8
Median Cost: $0.0

Total regular and recurring income
Mean Cost: $84,117.9
Median Cost: $66,420.0

Total irregular Income
Mean Cost: $18,423.7
Median Cost: $0.0

Total income all sources
Mean Cost: $86,736.3
Median Cost: $67,193.0

# Source

[stats.govt.nz](http://archive.stats.govt.nz/tools_and_services/releases_csv_files/csv-files-for-infoshare.aspx)