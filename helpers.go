package main

import (
	"sort"
	"strconv"

	"gonum.org/v1/gonum/stat"
)

func mean(array []float64) float64 {
	return stat.Mean(array, nil)
}

func median(array []float64) float64 {
	sort.Float64s(array)
	return stat.Quantile(0.5, stat.Empirical, array, nil)
}

func variance(array []float64) float64 {
	return stat.Variance(array, nil)
}

func stringarray2floatarray(array []string) []float64 {
	var floatarray []float64

	for _, value := range array {

		floatarray = append(floatarray, string2float(value))
	}

	return floatarray
}

func string2float(stringvar string) float64 {

	f, _ := strconv.ParseFloat(stringvar, 64)

	return f
}

func regionid(varible string) string {
	region := map[string]string{
		"AUK": "Auckland region",
		"WEL": "Wellington region",
		"CAN": "Canterbury region",
		"RNI": "Rest of North Island",
		"RSI": "Rest of South Island",
		"TNZ": "Total New Zealand",
	}

	return region[varible]
}

func sortmapbykey(array []int64) []int {

	var keys []int
	for _, k := range array {
		keys = append(keys, int(k))
	}
	sort.Ints(keys)
	return keys
}

func string2int(varible string) int64 {
	value, _ := strconv.ParseInt(varible, 10, 64)

	return value
}
