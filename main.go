package main

import (
	"encoding/csv"
	"fmt"
	"os"
	"strconv"

	"github.com/leekchan/accounting"
	"github.com/olekukonko/tablewriter"
)

type CsvLine struct {
	Table             string
	Year              string
	Region            string
	IncomeCode        string
	IncomeDescription string
	Mean              string
	MeanFlag          string
	MeanRse           string
	Median            string
	MedianFlag        string
	MedianRse         string
}

type stats struct {
	Mean   []float64
	Median []float64
}

func main() {
	fmt.Println("Average and median annual household income, by income source, for households receiving income from this source, year ended June 2017")
	table()
	summary()
}

func table() {
	table, _ := tablewriter.NewCSV(os.Stdout, "HES2017-Table1and2.csv", true)
	table.SetAlignment(tablewriter.ALIGN_LEFT) // Set Alignment
	table.SetAutoMergeCells(true)
	table.Render()
}

func summary() {
	file, err := os.Open("HES2017-Table1and2.csv")

	if err != nil {
		// err is printable
		// elements passed are separated by space automatically
		fmt.Println("Error:", err)
		return
	}
	defer file.Close()

	// Read File into a Variable
	lines, err := csv.NewReader(file).ReadAll()
	if err != nil {
		panic(err)
	}

	var datas = []CsvLine{}

	// Loop through lines & turn into object
	for id, line := range lines {
		if id != 0 {
			data := CsvLine{
				Table:             line[0],
				Year:              line[1],
				Region:            line[2],
				IncomeCode:        line[3],
				IncomeDescription: line[4],
				Mean:              line[5],
				MeanFlag:          line[6],
				MeanRse:           line[7],
				Median:            line[8],
				MedianFlag:        line[9],
				MedianRse:         line[10],
			}
			datas = append(datas, data)
		}
	}

	//summary
	fmt.Println("Summary:")
	fmt.Println("\nOverall:")
	ac := accounting.Accounting{Symbol: "$", Precision: 1}
	var array []float64
	for _, line := range datas {
		varfloat, _ := strconv.ParseFloat(line.Mean, 64)
		array = append(array, varfloat)
	}
	fmt.Printf("Mean All House Cost: %v\n", ac.FormatMoney(mean(array)))
	array = nil
	for _, line := range datas {
		varfloat, _ := strconv.ParseFloat(line.Mean, 64)
		array = append(array, varfloat)
	}
	fmt.Printf("Median All House Cost: %v\n", ac.FormatMoney(median(array)))
	array = nil
	fmt.Println("\nBy Country:")
	//get Region
	regioncollection := make(map[string]stats)
	statsvar := stats{}
	for _, line := range datas {
		statsvar.Mean = append(regioncollection[line.Region].Mean, string2float(line.Mean))
		statsvar.Median = append(regioncollection[line.Region].Median, string2float(line.Median))
		regioncollection[line.Region] = statsvar
	}
	//Sort

	// Print
	for id, value := range regioncollection {
		fmt.Printf("\n%s\nMean Cost: %s\nMedian Cost: %s\n", regionid(id), ac.FormatMoney(mean(value.Mean)), ac.FormatMoney(median(regioncollection[id].Median)))
	}
	//by year
	yearcollection := make(map[int64]stats)
	statsvar = stats{}
	fmt.Println("\nBy Year:")
	for _, line := range datas {
		year := string2int(line.Year)
		statsvar.Mean = append(yearcollection[year].Mean, string2float(line.Mean))
		statsvar.Median = append(yearcollection[year].Median, string2float(line.Median))
		yearcollection[year] = statsvar
	}

	for id, value := range yearcollection {
		fmt.Printf("\n%d\nMean Cost: %s\nMedian Cost: %s\n", id, ac.FormatMoney(mean(value.Mean)), ac.FormatMoney(median(yearcollection[id].Median)))
	}
	fmt.Println("\nBy Type:")
	//by Income
	incomecollection := make(map[string]stats)
	statsvar = stats{}

	for _, line := range datas {
		statsvar.Mean = append(incomecollection[line.IncomeDescription].Mean, string2float(line.Mean))
		statsvar.Median = append(incomecollection[line.IncomeDescription].Median, string2float(line.Median))
		incomecollection[line.IncomeDescription] = statsvar
	}
	for id, value := range incomecollection {
		fmt.Printf("\n%s\nMean Cost: %s\nMedian Cost: %s\n", id, ac.FormatMoney(mean(value.Mean)), ac.FormatMoney(median(value.Median)))
	}
}
